FROM registry.fedoraproject.org/fedora:27

LABEL name="kubernetes-kubelet" \
      version="v1.11.0" \
      architecture="x86_64" \
      atomic.type='system' \
      maintainer="Spyros Trigazis <spyridon.trigazis@cern.ch>"

COPY rpms /opt/rpms
COPY launch.sh /usr/bin/kubelet-docker.sh
COPY manifest.json tmpfiles.template service.template config.json.template /exports/
COPY config kubelet /etc/kubernetes/

# Containerized kubelet requires nsenter
RUN dnf install -y --setopt=tsflags=nodocs \
    /opt/rpms/*x86_64.rpm \
    util-linux ethtool systemd-udev e2fsprogs xfsprogs && dnf clean all

RUN mkdir -p /exports/hostfs/etc/cni/net.d && \
    mkdir -p /exports/hostfs/etc/kubernetes && \
    cp /etc/kubernetes/{config,kubelet} /exports/hostfs/etc/kubernetes && \
    mkdir -p /exports/hostfs/usr/local/bin/ && \
    cp /usr/bin/kubectl /exports/hostfs/usr/local/bin/kubectl && \
    cp /usr/bin/kubeadm /exports/hostfs/usr/local/bin/kubeadm

ENTRYPOINT ["/usr/bin/kubelet-docker.sh"]
